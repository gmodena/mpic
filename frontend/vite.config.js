import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      script: {
        /* The defineModel macro ships with Vue 3.4 but
           because we are limited by Codex to use Vue 3.3.9,
           we have to explicitly enable defineModel here for
           the version of Vue we are running.
        */
        defineModel: true
      }
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    outDir: '../public'
  }
})
