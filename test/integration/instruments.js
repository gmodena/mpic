'use strict';

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../app').app;

chai.use(chaiHttp);
chai.should();

describe('Instruments', () => {
    describe('GET /instruments', () => {
        it('should get all instruments', (done) => {
            chai.request(app)
                .get('/instruments')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    describe('GET /instrument/web-scroll-ui', () => {
        it('should get an instrument call web-scroll-ui', (done) => {
            chai.request(app)
                .get('/instrument/web-scroll-ui')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });
});
