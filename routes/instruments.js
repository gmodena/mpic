'use strict';

const express = require('express');
const { validationResult } = require('express-validator');
const { addInstrument, getInstrument, getInstruments } = require('../controller/instrumentController');
const { instrumentValidator } = require('./validators');
const router = express.Router();

router.get('/instruments', (req, res) => {
	getInstruments(req, res);
});

// No auth required for this endpoint
router.get('/api/v1/instruments', (req, res) => {
	getInstruments(req, res);
});

router.get('/instrument/:slug', (req, res) => {
	// TODO Sanitize slug
	getInstrument(req, res);
});

router.post('/instruments',
	instrumentValidator,
	async (req, res) => {
		req.app.logger.info(req);
		// TODO Validate the double-submit cookie

		// Form validation
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			req.app.logger.error('There are some errors while validating the form');
			return res.status(400).json({
				status: 400,
				type: 'bad-request',
				title: 'There are some errors while validating the form',
				detail: errors.array()
			});
		}

		addInstrument(req, res);
	}
);

module.exports = router;
