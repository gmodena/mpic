# Metrics Platform Instrument Configurator (MPIC)

Metrics Platform Instrument Configuration service

## Requirements

* Node.js 18

## Getting Started

By default the service will look for a `config.yaml` file to load configuration so, create a `config.yaml` local file just creating a symlink to config file you want to use. To develop locally the best idea is to create a `config.local.yaml` one.

```
ln -s config.dev.yaml config.yaml`
```

Install the dependencies for backend and frontend:

```
npm install
```

Build the frontend (anyway this step can be skipped because there is a `prestart` script to do it automatically everytime we run the service):

```
npm run build-frontend
```

### Development environment

There is a `docker-compose.dev.yaml` file to build a container that will run a development database with some data already
included. If you want to use it while developing, just run:

```
docker compose -f docker-compose.dev.yaml up -d
```

And you will have a MariaDB container running and listening to connections on the port 3306. The file `.env` contains the values
that are been used to create and configure the database and tables.

### Running the service

To start the server locally simply run:

```
node server.js
```

In the case you have your own `config.local.yaml` file, you can just run the service as follows: (this filename is already included at `.gitignore`)

```
node server.js --config config.local.yaml
``` 

## Available endpoints

In `routes/instruments.js` file:

* GET `http://localhost:8080`: You'll see the login page and a navigation bar to explore the whole tool
* GET 'http://localhost:8080/api/v1/instruments': Returns an array that contains the JSON representation of all registered instruments

## Docker

In order to generate the Docker image, the blubber file needs to be configured and [Buidkit](https://github.com/moby/buildkit) must be installed.

To create the Dockerfile (for development purposes):
```
docker build --target development -f .pipeline/blubber.yaml -t mpic .
```

You can change the target name in the above command to build the image according to any existing `variant` in the blubber file (for example: development, test or production)

To run the service as a docker container locally for the first time, run the following command:

```
docker run -d -p 8080:8080 --name mpic mpic
```

## Project structure

- `.pipeline`: Contains the `blubber.yaml` file to generate the Dockerfile for this service
- `config`: Configuration files for service and database
- `controller`: Controller. For now only `instrumentController` one
- `db`: Initialization script for database to populate when building the development environment
- `frontend`: Frontend source code
- `public`: Where the frontend is store after building it statically
- `routes`: Route files. For now only `instruments` one
- `test`: Test folder
  - `integration`: Integration tests cases
  - `unit`: Unit tests cases
- `util`: Util libraries
  - `salUtil.js`: A few functions to log to SAL
- `.env`: Environment properties for docker-compose projects
- `app.js`: Main application file
- `config.dev.yaml`: Configuration file for the development environment
- `config.prod.yaml`: Configuration file for the production environment
- `docker-compose.dev.yaml`: Docker compose file to build a full development environment (tool + database)
- `docker-compose.yaml`: Docker compose file to build a local development environment (just the database)
- `Dockerfile`: Dockerfile to build the webapp
- `server.js`: Main backend file to launch the service

## Tests

To fire the test suite up (and see the test coverage), simply run:

```
npm test
```

You can also build a docker image and run tests inside a docker container:

To build the image:

```
docker build --target test -f .pipeline/blubber.yaml -t mpic-test .
```

To run the container for the first time:

```
docker run --name mpic-test mpic-test
```

## Troubleshooting

In a lot of cases when there is an issue with node it helps to recreate the
`node_modules` directory:

```
rm -r node_modules
fresh-node
npm install
```
