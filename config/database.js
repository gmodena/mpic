'use strict';

const config = require('./configuration');
const logger = require('pino')();

const dbConfig = {
	host: config.conf.database.host,
	user: config.conf.database.user,
	password: config.conf.database.password,
	database: config.conf.database.database
};

const db = require('knex')({
	client: config.conf.database.engine,
	connection: dbConfig
});

logger.info('Connected to the database at ' + dbConfig.host + ':' + dbConfig.database);

exports.db = db;
